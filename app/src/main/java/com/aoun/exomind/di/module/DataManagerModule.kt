package com.aoun.exomind.di.module

import com.aoun.exomind.dataManager.IWeatherDataManager
import com.aoun.exomind.dataManager.WeatherDataManager
import com.aoun.exomind.network.provider.weather.IWeatherApiProvider
import dagger.Module
import dagger.Provides

@Module
class DataManagerModule {

    @Provides
    fun provideWeatherDataManager(weatherApiProvider: IWeatherApiProvider): IWeatherDataManager =
        WeatherDataManager(weatherApiProvider)
}