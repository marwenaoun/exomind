package com.aoun.exomind.di.module

import com.aoun.exomind.presentation.home.HomeFragment
import com.aoun.exomind.presentation.weather.WeatherFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentModule {

    @ContributesAndroidInjector
    internal abstract fun contributeHomeFragment(): HomeFragment

    @ContributesAndroidInjector
    internal abstract fun contributeWeatherFragment(): WeatherFragment
}