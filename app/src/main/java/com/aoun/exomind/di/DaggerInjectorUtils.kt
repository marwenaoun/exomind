package com.aoun.exomind.di

import com.aoun.exomind.ExomindApp
import com.aoun.exomind.di.component.ApplicationComponent
import com.aoun.exomind.di.component.DaggerApplicationComponent

object DaggerInjectorUtils {

    var applicationComponent: ApplicationComponent? = null

    fun init(application: ExomindApp): ApplicationComponent {
        applicationComponent = DaggerApplicationComponent.builder().application(application).build()
            .apply { inject(application) }
        return applicationComponent!!
    }
}