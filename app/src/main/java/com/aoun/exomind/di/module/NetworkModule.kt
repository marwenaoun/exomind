package com.aoun.exomind.di.module

import com.aoun.exomind.BuildConfig.API_WEATHER_BASE_URL
import com.aoun.exomind.network.provider.weather.IWeatherApiProvider
import com.aoun.exomind.network.provider.weather.WeatherApiProvider
import com.aoun.exomind.network.provider.weather.WeatherApiService
import com.facebook.stetho.okhttp3.StethoInterceptor
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    fun provideOkHttpClient(
        stethoInterceptor: StethoInterceptor,
    ): OkHttpClient {
        return OkHttpClient.Builder()
            .addNetworkInterceptor(stethoInterceptor)
            .addNetworkInterceptor { chain ->
                val request = chain.request()
                val requestBuilder = request.newBuilder()
                chain.proceed(requestBuilder.build())
            }
            .build()
    }

    @Provides
    fun provideApiService(okHttpClient: OkHttpClient): WeatherApiService {
        return Retrofit.Builder().baseUrl(API_WEATHER_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .build()
            .create(WeatherApiService::class.java)
    }

    @Singleton
    @Provides
    fun provideWeatherApiProvider(apiService: WeatherApiService): IWeatherApiProvider {
        return WeatherApiProvider(apiService)
    }

    @Provides
    fun provideStethoInterceptor() = StethoInterceptor()
}