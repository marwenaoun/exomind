package com.aoun.exomind.presentation.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.viewbinding.ViewBinding
import com.aoun.exomind.presentation.MainActivity
import dagger.android.support.DaggerFragment


abstract class BaseFragment<VB : ViewBinding> : DaggerFragment() {
    private var _binding: VB? = null
    val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = getViewBinding()
        return binding.root
    }

    protected fun bindNavigation(viewModel: BaseViewModel) {
        viewModel.navigation.observe(
            viewLifecycleOwner,
            { navDirection -> findNavController().navigate(navDirection) })
    }


    fun getAppViewModelFactory(): ViewModelProvider.Factory =
        (activity as MainActivity).viewModelFactory

    abstract fun getViewBinding(): VB
}