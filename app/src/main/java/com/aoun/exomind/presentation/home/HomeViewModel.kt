package com.aoun.exomind.presentation.home

import com.aoun.exomind.presentation.base.BaseViewModel
import javax.inject.Inject

class HomeViewModel @Inject constructor() : BaseViewModel() {

    fun navigateToWeatherScreen() {
        navigate(HomeFragmentDirections.actionNavigateToWeatherFragment())
    }
}