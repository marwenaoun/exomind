package com.aoun.exomind.presentation.base

sealed class DataWrapper<out T>

data class Success<out T>(val data: T) : DataWrapper<T>()
data class Failure<out T>(val errorType: ErrorType, val throwable: Throwable? = null) :
    DataWrapper<T>()

interface ErrorType

