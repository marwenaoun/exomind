package com.aoun.exomind.presentation.weather

import android.os.Bundle
import android.util.Log
import android.view.View
import com.aoun.exomind.databinding.FragmentWeatherBinding
import com.aoun.exomind.misc.vm
import com.aoun.exomind.presentation.base.BaseFragment
import com.aoun.exomind.presentation.base.Failure
import com.aoun.exomind.presentation.base.Success
import com.aoun.exomind.presentation.weather.adapter.WeatherAdapter

private const val TAG = "WeatherFragment"

class WeatherFragment : BaseFragment<FragmentWeatherBinding>() {

    private val viewModel by lazy { vm(getAppViewModelFactory(), WeatherViewModel::class) }
    private val weatherAdapter: WeatherAdapter by lazy { WeatherAdapter() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindNavigation(viewModel)
        observeLiveData()
        initRecyclerView()
        viewModel.getWeather()
    }

    private fun observeLiveData() {
        with(viewModel) {
            weatherLiveData.observe(viewLifecycleOwner, { dataWrapper ->
                when (dataWrapper) {
                    is Success -> weatherAdapter.addItem(dataWrapper.data)
                    is Failure -> {
                        Log.d(TAG, "handle error " + dataWrapper.errorType)
                    }
                }
            })

            statusLiveData.observe(viewLifecycleOwner, {

            })
        }

    }

    private fun initRecyclerView() {
        binding.weatherRecyclerView.adapter = weatherAdapter
    }

    override fun getViewBinding() = FragmentWeatherBinding.inflate(layoutInflater)
}