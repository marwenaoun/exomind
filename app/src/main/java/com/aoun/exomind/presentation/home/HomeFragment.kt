package com.aoun.exomind.presentation.home

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import com.aoun.exomind.databinding.FragmentHomeBinding
import com.aoun.exomind.misc.vm
import com.aoun.exomind.presentation.base.BaseFragment

class HomeFragment : BaseFragment<FragmentHomeBinding>() {

    private val viewModel by lazy { vm(getAppViewModelFactory(), HomeViewModel::class) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindNavigation(viewModel)
        bindListeners()
    }

    private fun bindListeners() {
        binding.clickMeButton.setOnClickListener { viewModel.navigateToWeatherScreen() }
    }

    override fun getViewBinding() = FragmentHomeBinding.inflate(layoutInflater)
}