package com.aoun.exomind.presentation.weather

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.aoun.exomind.dataManager.IWeatherDataManager
import com.aoun.exomind.model.Weather
import com.aoun.exomind.presentation.base.*
import io.reactivex.Observable
import java.util.concurrent.TimeUnit
import javax.inject.Inject

private const val WEATHER_INTERVAL_INITIAL_DELAY = 0L
private const val WEATHER_INTERVAL_PERIOD = 10L
private const val WEATHER_INTERVAL_QUERIES_COUNT = 6L

class WeatherViewModel @Inject constructor(private val weatherDataManager: IWeatherDataManager) :
    BaseViewModel() {

    private val _weatherLiveData = MutableLiveData<DataWrapper<Weather>>()
    val weatherLiveData: LiveData<DataWrapper<Weather>> get() = _weatherLiveData

    private val _statusLiveData = MutableLiveData<DataWrapper<String>>()
    val statusLiveData: LiveData<DataWrapper<String>> get() = _statusLiveData

    fun getWeather() {
        val cityNames = arrayOf("Rennes", "Paris", "Nantes", "Bordeaux ", "Lyon", "Toulouse")

        Observable.interval(
            WEATHER_INTERVAL_INITIAL_DELAY,
            WEATHER_INTERVAL_PERIOD,
            TimeUnit.SECONDS
        )
            .take(WEATHER_INTERVAL_QUERIES_COUNT)
            .map {
                weatherDataManager.getWeather(cityNames[it.toInt()]).sub({ weather ->
                    _weatherLiveData.postValue(Success(weather))
                }, { throwable ->
                    _weatherLiveData.postValue(Failure(WeatherErrorType.WEATHER_ERROR, throwable))
                })
            }.subscribe()
    }

    enum class WeatherErrorType : ErrorType {
        WEATHER_ERROR
    }
}