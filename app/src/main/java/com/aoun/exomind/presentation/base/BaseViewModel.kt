package com.aoun.exomind.presentation.base

import androidx.lifecycle.ViewModel
import androidx.navigation.NavDirections
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import io.reactivex.internal.functions.Functions
import io.reactivex.schedulers.Schedulers

abstract class BaseViewModel : ViewModel() {

    val navigation: SingleLiveEvent<NavDirections> by lazy { SingleLiveEvent() }
    private val subscriptions: CompositeDisposable by lazy { CompositeDisposable() }

    fun <R> Single<R>.sub(
        onSuccess: Consumer<in R>? = Functions.emptyConsumer(),
        onError: Consumer<Throwable>? = Functions.ERROR_CONSUMER
    ) {
        async().subscribe(onSuccess, onError).autoDispose()
    }

    private fun <T> Single<T>.async(): Single<T> {
        return subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread())
    }

    private fun Disposable.autoDispose() {
        subscriptions.add(this)
    }

    protected fun navigate(directions: NavDirections) {
        navigation.postValue(directions)
    }

    override fun onCleared() {
        super.onCleared()
        subscriptions.dispose()
    }
}