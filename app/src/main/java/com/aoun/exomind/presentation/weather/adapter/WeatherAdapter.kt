package com.aoun.exomind.presentation.weather.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.aoun.exomind.databinding.ItemWeatherAdapterBinding
import com.aoun.exomind.model.Weather

class WeatherAdapter : RecyclerView.Adapter<WeatherAdapter.WeatherViewHolder>() {

    var items: ArrayList<Weather> = arrayListOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeatherViewHolder {
        return WeatherViewHolder(
            ItemWeatherAdapterBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: WeatherViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int = items.size

    fun addItem(weather: Weather) {
        items.add(weather)
        notifyItemInserted(itemCount - 1)
    }

    inner class WeatherViewHolder(private val binding: ItemWeatherAdapterBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(weather: Weather) {
            with(binding) {
                nameTextView.text = weather.name
                temperatureTextView.text = weather.temperature.toString()
                cloudCoverTextView.text = weather.cloudCover.toString()
            }
        }
    }
}