package com.aoun.exomind.dataManager

import com.aoun.exomind.model.Weather
import com.aoun.exomind.network.provider.weather.IWeatherApiProvider
import io.reactivex.Single

class WeatherDataManager(private val weatherApiProvider: IWeatherApiProvider) :
    IWeatherDataManager {

    override fun getWeather(cityName: String): Single<Weather> {
        return weatherApiProvider.getWeather(cityName)
    }
}

interface IWeatherDataManager {
    fun getWeather(cityName: String): Single<Weather>
}