package com.aoun.exomind

import android.content.Context
import androidx.multidex.MultiDex
import com.aoun.exomind.di.DaggerInjectorUtils
import com.facebook.stetho.Stetho
import dagger.android.DaggerApplication

class ExomindApp : DaggerApplication() {

    override fun onCreate() {
        super.onCreate()
        Stetho.initializeWithDefaults(this)
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun applicationInjector() = DaggerInjectorUtils.init(this)
}