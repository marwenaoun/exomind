package com.aoun.exomind.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Weather(
    val name: String,
    val temperature: Double,
    val cloudCover: Int
) : Parcelable