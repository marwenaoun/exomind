package com.aoun.exomind.misc

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import kotlin.reflect.KClass

inline fun <reified VM : ViewModel> Fragment.vm(
    factory: ViewModelProvider.Factory,
    model: KClass<VM>
) = ViewModelProviders.of(this, factory).get(model.java)