package com.aoun.exomind.network.provider.weather

import com.aoun.exomind.BuildConfig
import com.aoun.exomind.network.provider.weather.dto.WeatherDto
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApiService {

    @GET("data/2.5/weather")
    fun getWeather(
        @Query("q") cityName: String,
        @Query("appid") apiKey: String = BuildConfig.API_WEATHER_KEY
    ): Single<WeatherDto>
}