package com.aoun.exomind.network.provider.weather

import com.aoun.exomind.model.Weather
import com.aoun.exomind.network.provider.weather.mapper.WeatherMapper
import io.reactivex.Single

class WeatherApiProvider(private val weatherApiService: WeatherApiService) : IWeatherApiProvider {

    override fun getWeather(cityName: String): Single<Weather> {
        return weatherApiService.getWeather(cityName).map { WeatherMapper().mapWeather(it) }
    }
}

interface IWeatherApiProvider {
    fun getWeather(cityName: String): Single<Weather>
}