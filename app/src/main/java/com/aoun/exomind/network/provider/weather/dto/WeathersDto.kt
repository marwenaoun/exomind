package com.aoun.exomind.network.provider.weather.dto

import com.google.gson.annotations.SerializedName

data class WeathersDto(
	@SerializedName("id") val id: Int,
	@SerializedName("main") val main: String,
	@SerializedName("description") val description: String,
	@SerializedName("icon") val icon: String
)