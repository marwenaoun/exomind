package com.aoun.exomind.network.provider.weather.dto

import com.google.gson.annotations.SerializedName

data class CloudsDto(
	@SerializedName("all") val all: Int
)