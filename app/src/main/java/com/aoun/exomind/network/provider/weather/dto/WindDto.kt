package com.aoun.exomind.network.provider.weather.dto

import com.google.gson.annotations.SerializedName

data class WindDto(
	@SerializedName("speed") val speed: Double,
	@SerializedName("deg") val deg: Int
)