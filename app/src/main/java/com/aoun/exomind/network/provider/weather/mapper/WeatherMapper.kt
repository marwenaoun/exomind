package com.aoun.exomind.network.provider.weather.mapper

import com.aoun.exomind.model.Weather
import com.aoun.exomind.network.provider.weather.dto.WeatherDto

class WeatherMapper {

    fun mapWeather(weatherDto: WeatherDto): Weather {
        return Weather(
            name = weatherDto.name,
            temperature = weatherDto.main.temp,
            cloudCover = weatherDto.clouds.all
        )
    }
}